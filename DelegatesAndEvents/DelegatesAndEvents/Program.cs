﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

namespace DelegatesAndEvents
{
    class Program
    {
        static void Main(string[] args)
        {
            //FindMaxDemo();

            EventsDemo();
        }

        /// <summary>
        /// Метод демонстрирует работу функции расширения для поиска максимального значения в коллекции.
        /// </summary>
        private static void FindMaxDemo()
        {
            List<string> list = new List<string>();
            for (int i = 0; i < 10000; i++)
            {
                list.Add($"Value {i}");
            }

            string maxValue = list.GetMax(CalcHash);
            Console.WriteLine($"Максимальное значение равно {maxValue}");
        }

        private static int CalcHash(string value)
        {
            return value.GetHashCode();
        }

        /// <summary>
        /// Метод демонстрирует работу событий.
        /// </summary>
        private static void EventsDemo()
        {
            FileFinder finder = new FileFinder();
            finder.FileFound += onFileFound;
            finder.StopSearch += onSearchStopped;

            finder.CheckDirectory(@"C:\Temp\Samples\");
        }

        /// <summary>
        /// Метод выводит в консоль информацию о найденном файле.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="args"></param>
        private static void onFileFound(object obj, FileArgs args)
        {
            Console.WriteLine($"{DateTime.Now}: Найден файл {args.FileName}");
        }

        /// <summary>
        /// Метод выводит в консоль информацию о стоп-файле.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="args"></param>
        private static void onSearchStopped(object obj, FileArgs args)
        {
            if (args.isStopFile)
            {
                Console.WriteLine($"{DateTime.Now}: Поиск файлов остановлен. Найден стоп-файл {args.FileName}");
            }
        }

    }


}
