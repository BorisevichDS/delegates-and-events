﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DelegatesAndEvents
{
    internal static class EnumerableExtension
    {
        public static T GetMax<T>(this IEnumerable<T> collection, Func<T, int> func) where T : class
        {
            KeyValuePair<int, T> maxCollectionElement = new KeyValuePair<int, T>(int.MinValue, default);

            foreach (var el in collection)
            {
                int funcValue = func(el);

                if (funcValue > maxCollectionElement.Key)
                {
                    maxCollectionElement = new KeyValuePair<int, T>(funcValue, el);
                }
            }
            return maxCollectionElement.Value;
        }
    }
}
