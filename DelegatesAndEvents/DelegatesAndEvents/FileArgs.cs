﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DelegatesAndEvents
{
    /// <summary>
    /// Аргументы события поиска файла.
    /// </summary>
    internal class FileArgs : EventArgs
    {
        /// <summary>
        /// Имя файла.
        /// </summary>
        public string FileName { get; }

        /// <summary>
        /// Признак остановки.
        /// </summary>
        public bool isStopFile { get; }

        public FileArgs(string fileName)
        {
            FileName = fileName;
        }
        public FileArgs(string fileName, bool isstopfile)
        {
            FileName = fileName;
            isStopFile = isstopfile;
        }
    }
}
