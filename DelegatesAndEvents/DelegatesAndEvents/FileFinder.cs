﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace DelegatesAndEvents
{
    /// <summary>
    /// Класс поиска файлов в каталоге.
    /// </summary>
    internal class FileFinder
    {
        /// <summary>
        /// Событие, возникающее при нахождении файла.
        /// </summary>
        public EventHandler<FileArgs> FileFound;

        /// <summary>
        /// Событие, возникающее когда поиск файлов остановлен.
        /// </summary>
        public EventHandler<FileArgs> StopSearch;

        /// <summary>
        /// Метод ищет файлы в указанной директории.
        /// </summary>
        /// <param name="path">Путь к директории для поиска.</param>
        public void CheckDirectory(string path)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(path);

            foreach (var file in directoryInfo.GetFiles())
            {
                if (file.Name.Contains("stop"))
                {
                    FileFound = null;
                    StopSearch?.Invoke(this, new FileArgs(file.Name, true));
                }
                else
                {
                    FileFound?.Invoke(this, new FileArgs(file.Name));
                }

            }
        }
    }
}
